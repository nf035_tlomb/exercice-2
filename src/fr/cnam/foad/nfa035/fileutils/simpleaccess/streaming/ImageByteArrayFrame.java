package fr.cnam.foad.nfa035.fileutils.simpleaccess.streaming;

import org.apache.commons.codec.binary.Base64InputStream;
import org.apache.commons.codec.binary.Base64OutputStream;

import java.io.*;

/**
 * Représente une trame d'image encodée à l'aide d'un {@link ByteArrayOutputStream}.
 * Cette classe étend {@link AbstractImageFrameMedia} et fournit des méthodes concrètes
 * pour obtenir des flux encodés en Base64 à partir d'un tableau de bytes.
 */
public class ImageByteArrayFrame extends AbstractImageFrameMedia<ByteArrayOutputStream>{

    /**
     * Constructeur qui initialise la trame d'image avec le tableau de bytes donné.
     *
     * @param byteArrayOutputStream Le tableau de bytes représentant l'image.
     */
    public ImageByteArrayFrame(ByteArrayOutputStream byteArrayOutputStream) {
        super(byteArrayOutputStream);
    }

    /**
     * Renvoie un {@link OutputStream} encodé en Base64 pour écrire l'image.
     *
     * @return un flux de sortie encodé en Base64
     * @throws IOException si une erreur se produit lors de la création du flux
     */
    @Override
    public OutputStream getEncodedImageOutput() throws IOException {
        return new Base64OutputStream(getChannel());
    }

    /**
     * Renvoie un {@link InputStream} encodé en Base64 pour lire l'image.
     *
     * @return un flux d'entrée encodé en Base64
     * @throws IOException si une erreur se produit lors de la création du flux
     */
    @Override
    public InputStream getEncodedImageInput() throws IOException {
        return new Base64InputStream(new ByteArrayInputStream(getChannel().toByteArray()));
    }
}
