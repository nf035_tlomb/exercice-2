package fr.cnam.foad.nfa035.fileutils.simpleaccess.streaming;

import java.io.IOException;

/**
 * Fournit une base abstraite pour la sérialisation d'images en streaming.
 * Cette classe fournit une implémentation par défaut pour la méthode de sérialisation,
 * tout en exigeant des sous-classes qu'elles définissent comment obtenir les flux
 * d'entrée et de sortie pour la source et le média, respectivement.
 *
 * @param <S> le type de la source de l'image (par exemple, un fichier)
 * @param <M> le type de média vers lequel l'image doit être sérialisée
 */
public abstract class AbstractStreamingImageSerializer<S, M> implements ImageStreamingSerializer<S, M> {

  /**
   * Implémente la sérialisation en transférant les données de la source vers le média
   * à l'aide des flux définis par les méthodes {@code getSourceInputStream} et
   * {@code getSerializingStream}.
   *
   * @param source la source de l'image à sérialiser
   * @param media le média vers lequel l'image doit être sérialisée
   * @throws IOException si une erreur d'entrée/sortie se produit pendant la sérialisation
   */
  @Override
  public final void serialize(S source, M media) throws IOException {
    getSourceInputStream(source).transferTo(getSerializingStream(media));
  }
}
