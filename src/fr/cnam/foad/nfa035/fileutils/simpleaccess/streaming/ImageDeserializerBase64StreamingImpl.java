package fr.cnam.foad.nfa035.fileutils.simpleaccess.streaming;

import org.apache.commons.codec.binary.Base64InputStream;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Implémentation de l'interface {@link ImageStreamingDeserializer} qui permet la désérialisation
 * d'images encodées en Base64 à l'aide d'instances de {@link ImageByteArrayFrame}.
 */
public class ImageDeserializerBase64StreamingImpl implements ImageStreamingDeserializer<ImageByteArrayFrame>{

    /**
     * Flux de sortie vers lequel l'image désérialisée sera écrite.
     */
    private final OutputStream sourceOutputStream;

    /**
     * Constructeur qui initialise le flux de sortie pour la désérialisation.
     *
     * @param sourceOutputStream Le flux de sortie où les données désérialisées seront écrites.
     */
    public ImageDeserializerBase64StreamingImpl(OutputStream sourceOutputStream) {
        this.sourceOutputStream = sourceOutputStream;
    }

    /**
     * Désérialise l'image encodée en Base64 représentée par l'objet {@link ImageByteArrayFrame} donné
     * et écrit les données désérialisées dans le flux de sortie spécifié lors de la construction.
     *
     * @param media L'objet représentant l'image encodée en Base64 à désérialiser.
     * @throws IOException Si une erreur de lecture/écriture se produit pendant la désérialisation.
     */
    @Override
    public void deserialize(ImageByteArrayFrame media) throws IOException {
        try(InputStream in = getDeserializingStream(media)){
            byte[] buffer = new byte[4096];
            int bytesRead;
            while((bytesRead=in.read(buffer))!=-1){
                sourceOutputStream.write(buffer,0,bytesRead);
            }
        }
    }

    /**
     * Renvoie le flux de sortie initialisé lors de la construction.
     *
     * @return Le flux de sortie pour la désérialisation.
     */
    @Override
    @SuppressWarnings("unchecked")
    public < T extends OutputStream > T getSourceOutputStream() {
        return (T) sourceOutputStream;
    }

    /**
     * Renvoie un {@link InputStream} encodé en Base64 pour lire l'objet {@link ImageByteArrayFrame} donné.
     *
     * @param media L'objet représentant l'image encodée en Base64 à lire.
     * @return Un flux d'entrée pour lire l'image encodée.
     * @throws IOException Si une erreur se produit lors de la création du flux d'entrée.
     */
    @Override
    @SuppressWarnings("unchecked")
    public <K extends InputStream> K getDeserializingStream(ImageByteArrayFrame media) throws IOException {
        return (K) new Base64InputStream(media.getEncodedImageInput());
    }
}
