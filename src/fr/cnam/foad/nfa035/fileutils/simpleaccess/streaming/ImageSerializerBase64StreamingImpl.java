package fr.cnam.foad.nfa035.fileutils.simpleaccess.streaming;
import org.apache.commons.codec.binary.Base64OutputStream;

import java.io.*;

/**
 * Implémentation de l'interface {@link ImageStreamingSerializer} pour sérialiser des images
 * en utilisant l'encodage Base64. Cette implémentation prend en charge la sérialisation
 * de fichiers image vers des instances de {@link ImageByteArrayFrame}.
 */
public class ImageSerializerBase64StreamingImpl implements ImageStreamingSerializer<File, ImageByteArrayFrame> {

    /**
     * Sérialise le fichier image source en un format encodé en Base64
     * et stocke le résultat dans l'objet {@link ImageByteArrayFrame} donné.
     *
     * @param source Le fichier image à sérialiser.
     * @param media  L'objet dans lequel stocker l'image sérialisée.
     * @throws IOException Si une erreur de lecture/écriture se produit pendant la sérialisation.
     */
    @Override
    public void serialize(File source, ImageByteArrayFrame media) throws IOException {
        try (InputStream in = getSourceInputStream(source);
             OutputStream out = getSerializingStream(media)) {
            byte[] buffer = new byte[4096];
            int bytesRead;
            while ((bytesRead = in.read(buffer)) != -1) {
                out.write(buffer, 0, bytesRead);
            }
        }
    }

    /**
     * Renvoie un {@link OutputStream} encodé en Base64 pour écrire dans l'objet {@link ImageByteArrayFrame} donné.
     *
     * @param media L'objet dans lequel écrire l'image sérialisée.
     * @return Un flux de sortie pour écrire l'image encodée.
     * @throws IOException Si une erreur se produit lors de la création du flux de sortie.
     */
    @Override
    @SuppressWarnings("unchecked")
    public <T extends OutputStream> T getSerializingStream(ImageByteArrayFrame media) throws IOException {
        return (T) new Base64OutputStream(media.getEncodedImageOutput());
    }

    /**
     * Renvoie un {@link InputStream} pour lire le fichier image source donné.
     *
     * @param source Le fichier image à lire.
     * @return Un flux d'entrée pour lire le fichier image.
     * @throws IOException Si une erreur se produit lors de la création du flux d'entrée.
     */
    @Override
    @SuppressWarnings("unchecked")
    public InputStream getSourceInputStream(File source) throws IOException {
        return new FileInputStream(source);
    }
}