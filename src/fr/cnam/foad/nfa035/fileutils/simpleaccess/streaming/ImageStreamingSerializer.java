package fr.cnam.foad.nfa035.fileutils.simpleaccess.streaming;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Interface définissant les méthodes nécessaires pour la sérialisation d'images en streaming.
 *
 * @param <S> Le type de source à partir de laquelle l'image sera lue.
 * @param <M> Le type de média dans lequel l'image sérialisée sera écrite.
 */
public interface ImageStreamingSerializer<S, M> {

    /**
     * Sérialise une image depuis une source spécifiée vers un média donné.
     *
     * @param source La source à partir de laquelle l'image sera lue.
     * @param media  Le média dans lequel l'image sérialisée sera écrite.
     * @throws IOException Si une erreur d'entrée/sortie se produit pendant la sérialisation.
     */
    void serialize(S source, M media) throws IOException;

    /**
     * Fournit un flux d'entrée pour lire la source de l'image.
     *
     * @param source La source de l'image à sérialiser.
     * @return Le flux d'entrée pour lire l'image.
     * @throws IOException Si une erreur d'entrée/sortie se produit en récupérant le flux d'entrée.
     */
    <K extends InputStream> K getSourceInputStream(S source) throws IOException;

    /**
     * Fournit un flux de sortie pour écrire l'image sérialisée dans le média.
     *
     * @param media Le média dans lequel l'image sérialisée sera écrite.
     * @return Le flux de sortie pour écrire l'image sérialisée.
     * @throws IOException Si une erreur d'entrée/sortie se produit en récupérant le flux de sortie.
     */
    <T extends OutputStream> T getSerializingStream(M media) throws IOException;
}