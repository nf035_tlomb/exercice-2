package fr.cnam.foad.nfa035.fileutils.simpleaccess.streaming;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Représente un support abstrait pour les trames d'images. Cette classe fournit
 * une base pour différentes manières de représenter les trames d'images, tout
 * en permettant une flexibilité sur la manière dont les données d'image peuvent
 * être accédées (par exemple, encodées ou décodées).
 *
 * @param <T> le type de canal par lequel la trame d'image est accessible
 */
public abstract class AbstractImageFrameMedia<T> {

    private T channel;

    /**
     * Constructeur par défaut.
     */
    protected AbstractImageFrameMedia() {
    }

    /**
     * Renvoie le canal actuel.
     *
     * @return le canal
     */
    public T getChannel() {
        return channel;
    }

    /**
     * Définit le canal.
     *
     * @param channel le nouveau canal
     */
    public void setChannel(T channel) {
        this.channel = channel;
    }

    /**
     * Constructeur avec paramètre.
     *
     * @param channel le canal initial
     */
    AbstractImageFrameMedia(T channel) {
        this.channel = channel;
    }

    /**
     * Renvoie un flux de sortie encodé pour l'image.
     *
     * @return le flux de sortie
     * @throws IOException si une erreur d'entrée/sortie se produit
     */
    public abstract OutputStream getEncodedImageOutput() throws IOException;

    /**
     * Renvoie un flux d'entrée encodé pour l'image.
     *
     * @return le flux d'entrée
     * @throws IOException si une erreur d'entrée/sortie se produit
     */
    public abstract InputStream getEncodedImageInput() throws IOException;
}