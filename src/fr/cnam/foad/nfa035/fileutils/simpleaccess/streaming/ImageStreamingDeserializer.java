package fr.cnam.foad.nfa035.fileutils.simpleaccess.streaming;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Interface définissant les méthodes nécessaires pour la désérialisation d'images.
 *
 * @param <M> Le type de média qui sera utilisé pour la désérialisation.
 */
public interface ImageStreamingDeserializer<M> {

    /**
     * Désérialise une image à partir d'un média spécifié.
     *
     * @param media Le média contenant l'image encodée à désérialiser.
     * @throws IOException Si une erreur d'entrée/sortie se produit pendant la désérialisation.
     */
    void deserialize(M media) throws IOException;

    /**
     * Fournit un flux d'entrée pour la désérialisation d'une image.
     *
     * @param media Le média à partir duquel le flux d'entrée sera obtenu.
     * @return Le flux d'entrée pour la désérialisation.
     * @throws IOException Si une erreur d'entrée/sortie se produit en récupérant le flux d'entrée.
     */
    <K extends InputStream> K getDeserializingStream(M media) throws IOException;

    /**
     * Fournit un flux de sortie pour stocker l'image désérialisée.
     *
     * @return Le flux de sortie pour stocker l'image.
     */
    <T extends OutputStream> T getSourceOutputStream();
}